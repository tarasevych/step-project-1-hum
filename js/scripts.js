$(document).ready(function(){

     //------------------- Section 3 --------------------//
    //------------------ Our Services ------------------//

    $('.services__navbar > li').click(function(currentNavbarId){
        currentNavbarId = $(this).attr('data-NavItemId');

        $('.services__navbar > li').removeClass('active');
        $('.service__description > li.active').removeClass('active');

        $(this).addClass('active');
        $('[data-DescItemId =' + currentNavbarId + ']').addClass('active');
    });

     //----------------- Section 5 ----------------------//
    //-------------- Our Amazing Work ------------------//

    $('.work__gallery').children('.work__gallery > div:gt(11)').addClass('hidden');

    $('.work__navbar > li').click(function(currentNavbarId){
        currentNavbarId = $(this).attr('data-workNavId');

        if (currentNavbarId === 'all'){
            $('.work__gallery > div').addClass('hidden');
            $('.work__gallery').children('.work__gallery > div:lt(12)').removeClass('hidden');
            $('.load-button').removeClass('hidden');

            $('.work__navbar > li').removeClass('work__nav-active');
            $(this).addClass('work__nav-active');
        }
        else {
            $('.work__navbar > li').removeClass('work__nav-active');
            $('.work__gallery > div').addClass('hidden');

            $(this).addClass('work__nav-active');
            $('[data-galerryId =' + currentNavbarId + ']').removeClass('hidden');
        }

        $('.load-button').click(function (){
            $('.work__gallery > div').removeClass('hidden');
            $('.load-button').addClass('hidden');

        });
    });

     //----------------- Section 7 -------------------//
    //------------------ About Hum ------------------//

    $('.slider__client').click(function (currentClientId) {
        currentClientId = $(this).attr('data-clientId');

        $('.slider__client').removeClass('active');
        $('.feedback > li.active').removeClass('active');

        $(this).addClass('active');
        $('[data-feedbackId =' + currentClientId + ']').addClass('active');
    });

    $('.next__slider-button').click(function (currentClient, currentClientId, currentFeedback, currentFeedbackId, nextClientId, nextFeedbackId) {
        currentClient = $('.slider__client.active');
        currentClientId = $('.slider__client.active').attr('data-clientId');

        currentFeedback = $('.feedback > li.active');
        currentFeedbackId = $('.feedback > li.active').attr('data-FeedbackId');

        nextClientId = +currentClientId +1;
        nextFeedbackId = +currentFeedbackId +1;

        if (currentClientId >= $('.feedback__slider li').length){
            nextClientId = 1;
            nextFeedbackId = 1;
        }

        $('.feedback').animate({opacity: 'hide'}, 500,
            function() {
                currentClient.removeClass('active');
                currentFeedback.removeClass('active');
                $('[data-clientId =' + nextClientId + ']').addClass('active');
                $('[data-feedbackId =' + nextFeedbackId + ']').addClass('active')
            }
        );

        $('.feedback').animate({opacity: 'show'}, 500);
    });

    $('.prev__slider-button').click(function (currentClient, currentClientId, currentFeedback, currentFeedbackId, prevClientId, prevFeedbackId) {
        currentClient = $('.slider__client.active');
        currentClientId = $('.slider__client.active').attr('data-clientId');

        currentFeedback = $('.feedback > li.active');
        currentFeedbackId = $('.feedback > li.active').attr('data-FeedbackId');

        prevClientId = +currentClientId - 1;
        prevFeedbackId = +currentFeedbackId - 1;

        if (currentClientId <= 1){
            prevClientId = $('.feedback__slider li').length;
            prevFeedbackId = $('.feedback__slider li').length;
        }

        $('.feedback').animate({opacity: 'hide'}, 500,
            function() {

                currentClient.removeClass('active');
                currentFeedback.removeClass('active');

                $('[data-clientId =' + prevClientId + ']').addClass('active');
                $('[data-feedbackId =' + prevFeedbackId + ']').addClass('active')
            });

        $('.feedback').animate({opacity: 'show'}, 500);
    });

    $('.slider__client').click(function (currentClientId) {
        currentClientId = $(this).attr('data-clientId');

        $('.slider__client').removeClass('active');
        $('.feedback > li.active').removeClass('active');

        $(this).addClass('active');
        $('[data-feedbackId =' + currentClientId + ']').addClass('active')
    });
});



